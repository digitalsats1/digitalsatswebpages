February 14, 2021 3:15:05 PM

Harry's vagrant OVA machine ubuntu20-box
   To use SSH from your host, issue:
      ssh vagrant@127.0.0.1 -p2204

      For RDP on both flavors, bring up the Microsoft RDP Client on
      either Windows or OSx, and connect to 127.0.0.1:33889. If you
      run on Linux, you can use the XRDP client.

      Both VMware and VirtualBox convert 127.0.0.1:2204 to port 22 on
      the guest's NAT address, and 127.0.0.1:33889 to port 3389 on the
      guest's NAT address.

--> Harry's OVA for GMAT development:
      /Volumes/LaCie3/fig/Users/kastner/Downloads/ubuntu20-box.ova
    -> Also at http://download.digitalsats.com
       a.k.a.
       curl -O http://sea.softcafe.net/digitalsats/download/ubuntu20-box.ova

   vagrant/vagrant
      /home/vagrant
         Desktop/GMATd.desktop
         GMAT_Install_Script_Logs/
         buildScripts/
         gmat/
         gmat-build/
      # General and empty
         Desktop
         Documents
         Downloads
         Music
         Pictures
         Public
         Templates
         Videos
         thinclient_drives

Harry's comments:
   I think if we're going to stay Open Source we need to keep our
   stuff on Sourceforge, GitHub or GitLab. GMAT is on Sourceforge, but
   some of the recent work is on GitLab. BTW, while GitLab limits file
   sizes to 20M, they suggest using GNU split to break up files and
   cat to put them back together. The OVA and box files are around 4G
   each. GitLab provides their code in an Open Source repository.

   If I could get everyone running Vagrant, I could automatically get
   the VM configured from my scripts during first boot and keep the
   download down to a couple of K. Of course depending on your
   hardware it could take five or six hours to run the scripts. The
   scripts could be automatically invoked during the first boot of the
   VM.

   You probably should forget about Fortran. The Fortran code is
   ancient and converted to C with the f2c package. The Python code
   sits on 2 sides of GMAT.

   Python application code -> Python API call to GMAT application <-
   GMAT application responds with hooks to full GMAT API that can be
   called from Python application, above -> GMAT script language -> <-
   CallPythonFunction
   [result(s)=Python.PythonModuleName.PythonFunctionName(inputParms)].

   For GMAT, it can be a stand alone system with the Python at both
   ends optionally. Of course, we're planning on using one or both
   sides as required.

   Would it help either of you if I provided a bunch of unsorted links
   I've bookmarked over the last months?
