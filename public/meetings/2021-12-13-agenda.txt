Agenda of Monday, December 13, 2021 6:30pm

Agenda:
1) Introductions
2) Brief History & Objectives
3) Tasks Before Us
4) How to Learn More
5) How to Volunteer
6) Next Meeting & Adjourn
