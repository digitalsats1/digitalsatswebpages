Meeting of Saturday, February 5, 2022 10:00am
   Don Black, Ph.D.
   Harry Goldschmitt
   John Kastner, Ph.D.
   Russell Lenahan
   Martin Martinez

Discussions:
   - Let's Encrypt for web sites.
   - GMAT mailing lists on SourceForge.net to join:
     For example, https://sourceforge.net/projects/gmat/lists/gmat-users
   - Additions to OVA for Jupyter and VNA.
   - Upcoming OpenFramesInterface for GMAT to be put in future OVA.
   - Starting a GMAT-DevOps group on GitLab.
   - Calling GMAT scripts without the IDE.

Action Items:
   - Don
       - Let's Encrypt security to be added to DigitalSats web site
         for https. John will assist.
   - Harry
       - Studying addition of Jupyter and VNC to OVA.
       - Creating a GMAT-DevOps group on GitLab for OVA discussions.
   - Martin
       - Continue to work on Jupyter integration into Harry's OVA.
       - Working on making Jupyter a service.
   - Russell
       - Studying internal GMAT attitude code and usage.
   - John
       - Trying to run GMAT script code from Python rather than within
         the IDE or Jupyter.
